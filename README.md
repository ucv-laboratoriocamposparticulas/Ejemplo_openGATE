Este repositorio contiene dos carpetas para simular ejemplos en GATE. 

**Objetivo**: realizar simulaciones en GATE usando neutrones. 

+ La carpeta **EjemploMWE** contiene todo lo necesario para realizar una simulación de práctica, irradiando un cilindro de agua contenido en una esfera de pulmón. 
+ La carpeta **EjemploGATE** contiene los archivos y documentos necesarios para correr una simulación como las realizadas en el trabajo **Efectos Radibiológicos de Neutrones en Grandes Altitudes**. 


Ambos ejemplos fueron desarrollados haciendo uso de la versión **vGATE8.0** (máquina virtual vGATE), la cual puede descargarse en el siguiente link: http://www.opengatecollaboration.org/


Pasos para una simulación:

1. Descargar la vGATE8.0
2. Descargar el repositorio
3. Seguir los pasos indicados para cada caso


### Nota ###

Para la realización de las gráficas, se deben instalar los siguientes paquetes en la máquina virtual:

Instalar pip y las siguientes libraries en python3.6

sudo apt-get install python3-pip

python3 -m pip install --upgrade pip

python3 -m pip install --upgrade Pillow

python3.6 -m pip install numpy

python3.6 -m pip install matplotlib

python3.6 -m pip install tikzplotlib
