import numpy as np
import math
#https://pypi.org/project/matplotlib2tikz/
from matplotlib import pyplot as plt
import tikzplotlib as tikz
plt.style.use("ggplot")
dim=30
escala=100 #reescalamiento de dosis. Si la muestra es del 1%, la dosis total debe ser 100 veces mayor
factor=5 #factor de ponderacion de la radiacion

def prepara_numpyarray(filaname):
    papiro = []
    with open(filaname) as my_file:
        # Este for salta las primeras 6 lineas de encabezado
        for i in range(6):
            next(my_file)
        # Este for continua leyendo el resto del archivo
        for line in my_file:
            if len(line.strip()) != 0: # Condición de linea (no) vacía
                l=line.strip().split(" ") # "Limpia" los extremos y la pica en pedazos separados por espacios
                l=list(map(float, l)) # Convierte los elementos en números
                papiro.append(l) # Agrega la fila de números al arreglo
    dim=len(papiro[0])
    if math.floor(dim/2) != dim/2:
        print("Warning! la longitud del cubo en voxels no es par")
    ladrillo = [] #Éste será el arrglo 3 dimensional
    print('# Imprimiendo los primeros 60 valores distintos de cero\n# (para verificar contra el archivo de texto e interpretar correctamente las dimensiones del arreglo)')
    print('# Glosario:')
    print('# Profundidad: se refiere a capas de voxels en la dirección del haz (cuenta los paños del archivo)')
    print('# Vertical: se refiere al número de lineas del archivo dentro de la capa')
    print('# Horizontal: se refiere a la columna del archivo')
    print('# lineaFile: es la linea donde debería verse el Valor impreso a la derecha, en la columna número:Horizontal')
    print('# Profundidad','Vertical','Horizontal','lineaFile','Valor')
    lines=0 # Número de valores diferentes de cero que se mostrarán en pantalla. Sirve para revisar el orden.
    for i in range(dim): # Este for contará la primera dimensión del arreglo (capas)
        capa = [] # Este arreglo 2 dimensional debe corresponder a cada corte en el archivo original
        for j in range(dim): # Este for recorre las lineas del archivo de dim en dim. Cada recorrido genera una capa del ladrillo
            capa.append(papiro[dim*i+j]) # Se van añadiendo las filas al arreglo 2d de voxels a profundidad i y verticalidad j
            for k in range(dim): #Este for reorre la linea que se ha agregado e imprime los primeros 60 valores diferentes de cero
                if papiro[dim*i+j][k] != 0 and lines<60:
                    print(i,j,k,dim*i+j+6+i+1,papiro[dim*i+j][k])
                    lines = lines + 1
        ladrillo.append(capa) # Se pega el arreglo 2d llamado capa, que corresponde al conjunto de voxels a profundidad i, al arreglo 3d llamado ladrillo
    ladrillo=np.array(ladrillo) #Se convierte a un tipo np.array para faciltar las operaciones    
    return(ladrillo)

# el arreglo b es el error estadístico relativo (ver el nombre del archivo prueba/PielBAMA-Dose-Uncertainty.txt)
b=prepara_numpyarray("Pecho/PechoSLAB-Dose-Uncertainty.txt")
# escala: reescalamiento de la dosis. Si la simulación se hizo con el 1% de las partículas -> la escala es 100
# a es la dosis simulada
a=factor*escala*prepara_numpyarray("Pecho/PechoSLAB-Dose.txt")
# c es el error absoluto en la dosis
c=a*b

def promedio_central_line(phantom):
    PromCentralLine = []
    dim=len(phantom[0])
    for i in range(dim-1):
        PromCentralLine.append((phantom[i][math.floor(dim/2)-1][math.floor(dim/2)-1] + phantom[i][math.floor(dim/2)-1][math.floor(dim/2)] + phantom[i][math.floor(dim/2)][math.floor(dim/2)-1] + phantom[i][math.floor(dim/2)][math.floor(dim/2)] +
                          phantom[i + 1][math.floor(dim/2)-1][math.floor(dim/2)-1] + phantom[i + 1][math.floor(dim/2)-1][math.floor(dim/2)] + phantom[i + 1][math.floor(dim/2)][math.floor(dim/2)-1] + phantom[i + 1][math.floor(dim/2)][math.floor(dim/2)])/8)
    return(PromCentralLine)

dosis_ambiental=promedio_central_line(a)
dosis_ambiental_err=promedio_central_line(c)
x=range(1,30)

tv_BAMa=11.42 #tiempo de vuelo Buenos Aires Madrid
dosis_ambiental0=np.asarray(dosis_ambiental)/tv_BAMa
dosis_ambiental_err0=np.asarray(promedio_central_line(c))/tv_BAMa

print('Dosis ambiental a 1 cm ',dosis_ambiental0[0])
print('Error absoluto en dosis ambiental a 1 cm ',dosis_ambiental_err0[0])


# el arreglo b es el error estadístico relativo (ver el nombre del archivo prueba/PielBAMA-Dose-Uncertainty.txt)
#b=prepara_numpyarray("BogBA/SLABPecho-Dose-Uncertainty.txt")
e=prepara_numpyarray("Piel/PielSLAB-Dose-Uncertainty.txt")
# escala: reescalamiento de la dosis. Si la simulación se hizo con el 1% de las partículas -> la escala es 100
# a es la dosis simulada
#a=escala*prepara_numpyarray("BogBA/SLABPecho-Dose.txt")
d=factor*escala*prepara_numpyarray("Piel/PielSLAB-Dose.txt")
# c es el error absoluto en la dosis
f=d*e

dosis_ambiental1=promedio_central_line(d)
dosis_ambiental_err1=promedio_central_line(f)
########
## REVISAR AQUI XXX
########
#tv_BogBA=11.42 #tiempo de vuelo BAMa
#dosis_ambiental11=dosis_ambiental1*tv_BogBA #Tasa de dosis ambiental
#dosis_ambiental_err11=promedio_central_line(f)*tv_BogBA
dosis_ambiental11=np.asarray(dosis_ambiental1)/tv_BAMa
dosis_ambiental_err11=np.asarray(promedio_central_line(f))/tv_BAMa

print('Dosis ambiental a 1 cm ',dosis_ambiental11[0])
print('Error absoluto en dosis ambiental a 1 cm ',dosis_ambiental_err11[0])




#https://www.tutorialspoint.com/numpy/numpy_matplotlib.htm
#https://jakevdp.github.io/PythonDataScienceHandbook/04.03-errorbars.html
plt.title("Comparación de tasa de dosis ambiental equivalente entre tejidos") 
plt.xlabel("Profundidad (cm)") 
plt.ylabel("Dosis (Sv)")#,labelpad=200) 
#plt.plot(x,dosis_ambiental)
Pecho=plt.errorbar(x, dosis_ambiental0, yerr=dosis_ambiental_err0, fmt='.b', label='Pecho')
BAMa=plt.yscale('log')
Piel=plt.errorbar(x, dosis_ambiental11, yerr=dosis_ambiental_err11, fmt='.', color='darkorange', label='Piel')
BAMa=plt.yscale('log')
plt.legend(handles=[Pecho, Piel])
#plt.show()
#tikz.save("prueba.tex")#, axis_height='7cm', axis_width='9cm')
tikz.get_tikz_code(figure='gcf', filepath="prueba2.tex", axis_width=None, axis_height=None, textsize=10.0, tex_relative_path_to_data=None, externalize_tables=False, override_externals=False, strict=False, wrap=True, add_axis_environment=True, extra_axis_parameters='every axis y label/.style={at={(ticklabel cs: 5,0)}, anchor=north}', extra_groupstyle_parameters={}, extra_tikzpicture_parameters=None, dpi=None, show_info=False, include_disclaimer=True, standalone=False, float_format='.15g', table_row_sep='\n', flavor='latex')
tikz.save("TDAE_pechopiel.tex", axis_height='10cm', axis_width='14cm')
#tikz.save("TDAE_pechopiel.png")
